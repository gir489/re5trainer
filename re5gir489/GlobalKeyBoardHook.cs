﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace re5gir489
{
    public class GlobalKeyboardHook
    {
        [DllImport("user32.dll")]
        static extern int CallNextHookEx(IntPtr hhk, int nCode, int wParam, ref KBDLLHOOKSTRUCT lParam);
        [DllImport("user32.dll")]
        static extern IntPtr SetWindowsHookEx(int idHook, LLKeyboardHook lpfn, IntPtr hMod, uint dwThreadId);
        [DllImport("user32.dll")]
        static extern bool UnhookWindowsHookEx(IntPtr hhk);
        [DllImport("user32.dll")]
        public static extern short GetAsyncKeyState(uint keyState);
        [DllImport("kernel32.dll")]
        static extern IntPtr LoadLibrary(string lpFileName);

        public delegate int LLKeyboardHook(int Code, int wParam, ref KBDLLHOOKSTRUCT lParam);

        public struct KBDLLHOOKSTRUCT
        {
            public UInt32 vkCode;
            public UInt32 scanCode;
            public UInt32 flags;
            public UInt32 time;
            public UInt64 dwExtraInfo;
        }

        const int WH_KEYBOARD_LL = 13;
        const int WM_KEYDOWN = 0x0100;
        const int WM_KEYUP = 0x0101;
        const int WM_SYSKEYDOWN = 0x0104;
        const int WM_SYSKEYUP = 0x0105;
        const int HC_ACTION = 0x0;
        readonly LLKeyboardHook llkh;
        public List<Keys> HookedKeys = new List<Keys>();

        IntPtr hhk = IntPtr.Zero;

        public event KeyEventHandler KeyDown;
        public event KeyEventHandler KeyUp;

        public bool CheckKeyRepeating;

        public GlobalKeyboardHook(bool OneKeyEventPerOneKeyPress = true)
        {
            llkh = new LLKeyboardHook(LowLevelKeyboardProc);
            CheckKeyRepeating = OneKeyEventPerOneKeyPress;
        }
        ~GlobalKeyboardHook()
        {
            UnHook();
        }

        public void Hook()
        {
            IntPtr hInstance = LoadLibrary("User32");
            hhk = SetWindowsHookEx(WH_KEYBOARD_LL, llkh, hInstance, 0);
        }

        public void UnHook()
        {
            UnhookWindowsHookEx(hhk);
        }

        public int LowLevelKeyboardProc(int nCode, int wParam, ref KBDLLHOOKSTRUCT lParam)
        {
            if (nCode == HC_ACTION)
            {
                Keys key = (Keys)lParam.vkCode;
                if (HookedKeys.Contains(key))
                {
                    KeyEventArgs kArg = new KeyEventArgs(key);
                    if ((GetAsyncKeyState(lParam.vkCode) == 0 || CheckKeyRepeating == false) && (wParam == WM_KEYDOWN || wParam == WM_SYSKEYDOWN))
                        KeyDown?.Invoke(this, kArg);
                    else if ((wParam == WM_KEYUP || wParam == WM_SYSKEYUP))
                        KeyUp?.Invoke(this, kArg);
                    if (kArg.Handled)
                        return 1;
                }
            }
            return CallNextHookEx(hhk, nCode, wParam, ref lParam);
        }
    }
}