﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace re5gir489
{
    public partial class HelpPlox : Form
    {
        public HelpPlox()
        {
            InitializeComponent();
        }

        private void btnCloseHelp_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblExplaination_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://youtu.be/qrXcgwzj0wU");
            this.lblExplaination.Links[0].Visited = true;
        }
    }
}
