﻿namespace re5gir489
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tmrMain = new System.Windows.Forms.Timer(this.components);
            this.lblWaiting = new System.Windows.Forms.Label();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.howDoIFuckingWorkThisThingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.cbxItemType = new System.Windows.Forms.ComboBox();
            this.lblItem = new System.Windows.Forms.Label();
            this.lblValue = new System.Windows.Forms.Label();
            this.txtValue = new System.Windows.Forms.TextBox();
            this.lblDamage = new System.Windows.Forms.Label();
            this.grpWpnStat = new System.Windows.Forms.GroupBox();
            this.txtPiercing = new System.Windows.Forms.TextBox();
            this.lblPiercing = new System.Windows.Forms.Label();
            this.txtCrit = new System.Windows.Forms.TextBox();
            this.lblCrit = new System.Windows.Forms.Label();
            this.txtCapMax = new System.Windows.Forms.TextBox();
            this.lblCapMax = new System.Windows.Forms.Label();
            this.txtCapacity = new System.Windows.Forms.TextBox();
            this.lblCapacity = new System.Windows.Forms.Label();
            this.txtReloadMax = new System.Windows.Forms.TextBox();
            this.lblReloadMax = new System.Windows.Forms.Label();
            this.txtReloadSpeed = new System.Windows.Forms.TextBox();
            this.lblReload = new System.Windows.Forms.Label();
            this.txtDmgMax = new System.Windows.Forms.TextBox();
            this.lblDamageMax = new System.Windows.Forms.Label();
            this.txtDamage = new System.Windows.Forms.TextBox();
            this.cbxAmmo = new System.Windows.Forms.CheckBox();
            this.cbxSwagBucks = new System.Windows.Forms.CheckBox();
            this.cbxMemeMode = new System.Windows.Forms.CheckBox();
            this.cbxSwagModeActivate = new System.Windows.Forms.CheckBox();
            this.menuStrip1.SuspendLayout();
            this.grpWpnStat.SuspendLayout();
            this.SuspendLayout();
            // 
            // tmrMain
            // 
            this.tmrMain.Enabled = true;
            this.tmrMain.Interval = 500;
            this.tmrMain.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblWaiting
            // 
            this.lblWaiting.AutoSize = true;
            this.lblWaiting.BackColor = System.Drawing.Color.Transparent;
            this.lblWaiting.ForeColor = System.Drawing.Color.Red;
            this.lblWaiting.Location = new System.Drawing.Point(199, 235);
            this.lblWaiting.Name = "lblWaiting";
            this.lblWaiting.Size = new System.Drawing.Size(87, 13);
            this.lblWaiting.TabIndex = 2;
            this.lblWaiting.Text = "Waiting for game";
            this.lblWaiting.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblWaiting.Click += new System.EventHandler(this.lblWaiting_Click);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.howDoIFuckingWorkThisThingToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.aboutToolStripMenuItem.Text = "&About...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // howDoIFuckingWorkThisThingToolStripMenuItem
            // 
            this.howDoIFuckingWorkThisThingToolStripMenuItem.Name = "howDoIFuckingWorkThisThingToolStripMenuItem";
            this.howDoIFuckingWorkThisThingToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.howDoIFuckingWorkThisThingToolStripMenuItem.Text = "How do I fucking work this thing?!";
            this.howDoIFuckingWorkThisThingToolStripMenuItem.Click += new System.EventHandler(this.howDoIFuckingWorkThisThingToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(301, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // cbxItemType
            // 
            this.cbxItemType.FormattingEnabled = true;
            this.cbxItemType.Location = new System.Drawing.Point(77, 44);
            this.cbxItemType.Name = "cbxItemType";
            this.cbxItemType.Size = new System.Drawing.Size(121, 21);
            this.cbxItemType.TabIndex = 4;
            this.cbxItemType.SelectionChangeCommitted += new System.EventHandler(this.cbxItemType_SelectionChangeCommitted);
            // 
            // lblItem
            // 
            this.lblItem.AutoSize = true;
            this.lblItem.BackColor = System.Drawing.Color.Transparent;
            this.lblItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblItem.ForeColor = System.Drawing.Color.Lime;
            this.lblItem.Location = new System.Drawing.Point(41, 47);
            this.lblItem.Name = "lblItem";
            this.lblItem.Size = new System.Drawing.Size(35, 13);
            this.lblItem.TabIndex = 5;
            this.lblItem.Text = "Item:";
            // 
            // lblValue
            // 
            this.lblValue.AutoSize = true;
            this.lblValue.BackColor = System.Drawing.Color.Transparent;
            this.lblValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValue.ForeColor = System.Drawing.Color.Lime;
            this.lblValue.Location = new System.Drawing.Point(33, 74);
            this.lblValue.Name = "lblValue";
            this.lblValue.Size = new System.Drawing.Size(43, 13);
            this.lblValue.TabIndex = 6;
            this.lblValue.Text = "Value:";
            // 
            // txtValue
            // 
            this.txtValue.Location = new System.Drawing.Point(77, 71);
            this.txtValue.MaxLength = 5;
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(50, 20);
            this.txtValue.TabIndex = 7;
            this.txtValue.TextChanged += new System.EventHandler(this.txtValue_TextChanged);
            // 
            // lblDamage
            // 
            this.lblDamage.AutoSize = true;
            this.lblDamage.BackColor = System.Drawing.Color.Transparent;
            this.lblDamage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDamage.ForeColor = System.Drawing.Color.Lime;
            this.lblDamage.Location = new System.Drawing.Point(6, 16);
            this.lblDamage.Name = "lblDamage";
            this.lblDamage.Size = new System.Drawing.Size(57, 13);
            this.lblDamage.TabIndex = 8;
            this.lblDamage.Text = "Damage:";
            // 
            // grpWpnStat
            // 
            this.grpWpnStat.BackColor = System.Drawing.Color.Transparent;
            this.grpWpnStat.Controls.Add(this.txtPiercing);
            this.grpWpnStat.Controls.Add(this.lblPiercing);
            this.grpWpnStat.Controls.Add(this.txtCrit);
            this.grpWpnStat.Controls.Add(this.lblCrit);
            this.grpWpnStat.Controls.Add(this.txtCapMax);
            this.grpWpnStat.Controls.Add(this.lblCapMax);
            this.grpWpnStat.Controls.Add(this.txtCapacity);
            this.grpWpnStat.Controls.Add(this.lblCapacity);
            this.grpWpnStat.Controls.Add(this.txtReloadMax);
            this.grpWpnStat.Controls.Add(this.lblReloadMax);
            this.grpWpnStat.Controls.Add(this.txtReloadSpeed);
            this.grpWpnStat.Controls.Add(this.lblReload);
            this.grpWpnStat.Controls.Add(this.txtDmgMax);
            this.grpWpnStat.Controls.Add(this.lblDamageMax);
            this.grpWpnStat.Controls.Add(this.txtDamage);
            this.grpWpnStat.Controls.Add(this.lblDamage);
            this.grpWpnStat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpWpnStat.ForeColor = System.Drawing.Color.White;
            this.grpWpnStat.Location = new System.Drawing.Point(36, 108);
            this.grpWpnStat.Name = "grpWpnStat";
            this.grpWpnStat.Size = new System.Drawing.Size(210, 119);
            this.grpWpnStat.TabIndex = 9;
            this.grpWpnStat.TabStop = false;
            this.grpWpnStat.Text = "Weapon Stats";
            // 
            // txtPiercing
            // 
            this.txtPiercing.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPiercing.Location = new System.Drawing.Point(159, 79);
            this.txtPiercing.MaxLength = 1;
            this.txtPiercing.Name = "txtPiercing";
            this.txtPiercing.Size = new System.Drawing.Size(34, 20);
            this.txtPiercing.TabIndex = 24;
            this.txtPiercing.TextChanged += new System.EventHandler(this.txtPiercing_TextChanged);
            // 
            // lblPiercing
            // 
            this.lblPiercing.AutoSize = true;
            this.lblPiercing.BackColor = System.Drawing.Color.Transparent;
            this.lblPiercing.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPiercing.ForeColor = System.Drawing.Color.Lime;
            this.lblPiercing.Location = new System.Drawing.Point(103, 82);
            this.lblPiercing.Name = "lblPiercing";
            this.lblPiercing.Size = new System.Drawing.Size(57, 13);
            this.lblPiercing.TabIndex = 23;
            this.lblPiercing.Text = "Piercing:";
            // 
            // txtCrit
            // 
            this.txtCrit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCrit.Location = new System.Drawing.Point(63, 79);
            this.txtCrit.MaxLength = 1;
            this.txtCrit.Name = "txtCrit";
            this.txtCrit.Size = new System.Drawing.Size(34, 20);
            this.txtCrit.TabIndex = 22;
            this.txtCrit.TextChanged += new System.EventHandler(this.txtCrit_TextChanged);
            // 
            // lblCrit
            // 
            this.lblCrit.AutoSize = true;
            this.lblCrit.BackColor = System.Drawing.Color.Transparent;
            this.lblCrit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCrit.ForeColor = System.Drawing.Color.Lime;
            this.lblCrit.Location = new System.Drawing.Point(6, 82);
            this.lblCrit.Name = "lblCrit";
            this.lblCrit.Size = new System.Drawing.Size(59, 13);
            this.lblCrit.TabIndex = 21;
            this.lblCrit.Text = "Critical%:";
            // 
            // txtCapMax
            // 
            this.txtCapMax.Enabled = false;
            this.txtCapMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCapMax.Location = new System.Drawing.Point(167, 50);
            this.txtCapMax.MaxLength = 2;
            this.txtCapMax.Name = "txtCapMax";
            this.txtCapMax.ReadOnly = true;
            this.txtCapMax.Size = new System.Drawing.Size(34, 20);
            this.txtCapMax.TabIndex = 20;
            // 
            // lblCapMax
            // 
            this.lblCapMax.AutoSize = true;
            this.lblCapMax.BackColor = System.Drawing.Color.Transparent;
            this.lblCapMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCapMax.ForeColor = System.Drawing.Color.Lime;
            this.lblCapMax.Location = new System.Drawing.Point(137, 53);
            this.lblCapMax.Name = "lblCapMax";
            this.lblCapMax.Size = new System.Drawing.Size(34, 13);
            this.lblCapMax.TabIndex = 19;
            this.lblCapMax.Text = "Max:";
            // 
            // txtCapacity
            // 
            this.txtCapacity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCapacity.Location = new System.Drawing.Point(97, 50);
            this.txtCapacity.MaxLength = 2;
            this.txtCapacity.Name = "txtCapacity";
            this.txtCapacity.Size = new System.Drawing.Size(34, 20);
            this.txtCapacity.TabIndex = 18;
            this.txtCapacity.TextChanged += new System.EventHandler(this.txtCapacity_TextChanged);
            // 
            // lblCapacity
            // 
            this.lblCapacity.AutoSize = true;
            this.lblCapacity.BackColor = System.Drawing.Color.Transparent;
            this.lblCapacity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCapacity.ForeColor = System.Drawing.Color.Lime;
            this.lblCapacity.Location = new System.Drawing.Point(6, 56);
            this.lblCapacity.Name = "lblCapacity";
            this.lblCapacity.Size = new System.Drawing.Size(60, 13);
            this.lblCapacity.TabIndex = 17;
            this.lblCapacity.Text = "Capacity:";
            // 
            // txtReloadMax
            // 
            this.txtReloadMax.Enabled = false;
            this.txtReloadMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReloadMax.Location = new System.Drawing.Point(167, 30);
            this.txtReloadMax.MaxLength = 2;
            this.txtReloadMax.Name = "txtReloadMax";
            this.txtReloadMax.ReadOnly = true;
            this.txtReloadMax.Size = new System.Drawing.Size(34, 20);
            this.txtReloadMax.TabIndex = 16;
            // 
            // lblReloadMax
            // 
            this.lblReloadMax.AutoSize = true;
            this.lblReloadMax.BackColor = System.Drawing.Color.Transparent;
            this.lblReloadMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReloadMax.ForeColor = System.Drawing.Color.Lime;
            this.lblReloadMax.Location = new System.Drawing.Point(137, 33);
            this.lblReloadMax.Name = "lblReloadMax";
            this.lblReloadMax.Size = new System.Drawing.Size(34, 13);
            this.lblReloadMax.TabIndex = 15;
            this.lblReloadMax.Text = "Max:";
            // 
            // txtReloadSpeed
            // 
            this.txtReloadSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReloadSpeed.Location = new System.Drawing.Point(97, 30);
            this.txtReloadSpeed.MaxLength = 2;
            this.txtReloadSpeed.Name = "txtReloadSpeed";
            this.txtReloadSpeed.Size = new System.Drawing.Size(34, 20);
            this.txtReloadSpeed.TabIndex = 14;
            this.txtReloadSpeed.TextChanged += new System.EventHandler(this.txtReloadSpeed_TextChanged);
            // 
            // lblReload
            // 
            this.lblReload.AutoSize = true;
            this.lblReload.BackColor = System.Drawing.Color.Transparent;
            this.lblReload.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReload.ForeColor = System.Drawing.Color.Lime;
            this.lblReload.Location = new System.Drawing.Point(6, 37);
            this.lblReload.Name = "lblReload";
            this.lblReload.Size = new System.Drawing.Size(91, 13);
            this.lblReload.TabIndex = 13;
            this.lblReload.Text = "Reload Speed:";
            // 
            // txtDmgMax
            // 
            this.txtDmgMax.Enabled = false;
            this.txtDmgMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDmgMax.Location = new System.Drawing.Point(167, 10);
            this.txtDmgMax.MaxLength = 2;
            this.txtDmgMax.Name = "txtDmgMax";
            this.txtDmgMax.ReadOnly = true;
            this.txtDmgMax.Size = new System.Drawing.Size(34, 20);
            this.txtDmgMax.TabIndex = 12;
            // 
            // lblDamageMax
            // 
            this.lblDamageMax.AutoSize = true;
            this.lblDamageMax.BackColor = System.Drawing.Color.Transparent;
            this.lblDamageMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDamageMax.ForeColor = System.Drawing.Color.Lime;
            this.lblDamageMax.Location = new System.Drawing.Point(137, 12);
            this.lblDamageMax.Name = "lblDamageMax";
            this.lblDamageMax.Size = new System.Drawing.Size(34, 13);
            this.lblDamageMax.TabIndex = 11;
            this.lblDamageMax.Text = "Max:";
            // 
            // txtDamage
            // 
            this.txtDamage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDamage.Location = new System.Drawing.Point(97, 10);
            this.txtDamage.MaxLength = 2;
            this.txtDamage.Name = "txtDamage";
            this.txtDamage.Size = new System.Drawing.Size(34, 20);
            this.txtDamage.TabIndex = 10;
            this.txtDamage.TextChanged += new System.EventHandler(this.txtDamage_TextChanged);
            // 
            // cbxAmmo
            // 
            this.cbxAmmo.AutoSize = true;
            this.cbxAmmo.BackColor = System.Drawing.Color.Transparent;
            this.cbxAmmo.Enabled = false;
            this.cbxAmmo.ForeColor = System.Drawing.Color.Red;
            this.cbxAmmo.Location = new System.Drawing.Point(36, 235);
            this.cbxAmmo.Name = "cbxAmmo";
            this.cbxAmmo.Size = new System.Drawing.Size(160, 17);
            this.cbxAmmo.TabIndex = 10;
            this.cbxAmmo.Text = "CTRL+NUM1: Infinite Ammo";
            this.cbxAmmo.UseVisualStyleBackColor = false;
            this.cbxAmmo.CheckedChanged += new System.EventHandler(this.cbxAmmo_CheckedChanged);
            // 
            // cbxSwagBucks
            // 
            this.cbxSwagBucks.AutoSize = true;
            this.cbxSwagBucks.BackColor = System.Drawing.Color.Transparent;
            this.cbxSwagBucks.Enabled = false;
            this.cbxSwagBucks.ForeColor = System.Drawing.Color.Red;
            this.cbxSwagBucks.Location = new System.Drawing.Point(36, 258);
            this.cbxSwagBucks.Name = "cbxSwagBucks";
            this.cbxSwagBucks.Size = new System.Drawing.Size(203, 17);
            this.cbxSwagBucks.TabIndex = 11;
            this.cbxSwagBucks.Text = "CTRL+NUM2: Swag Bucks 420Blazit";
            this.cbxSwagBucks.UseVisualStyleBackColor = false;
            this.cbxSwagBucks.CheckedChanged += new System.EventHandler(this.cbxSwagBucks_CheckedChanged);
            // 
            // cbxMemeMode
            // 
            this.cbxMemeMode.AutoSize = true;
            this.cbxMemeMode.BackColor = System.Drawing.Color.Transparent;
            this.cbxMemeMode.Enabled = false;
            this.cbxMemeMode.ForeColor = System.Drawing.Color.Red;
            this.cbxMemeMode.Location = new System.Drawing.Point(36, 281);
            this.cbxMemeMode.Name = "cbxMemeMode";
            this.cbxMemeMode.Size = new System.Drawing.Size(220, 17);
            this.cbxMemeMode.TabIndex = 12;
            this.cbxMemeMode.Text = "CTRL+NUM3: 3SPOOKY5MEME MODE";
            this.cbxMemeMode.UseVisualStyleBackColor = false;
            this.cbxMemeMode.CheckedChanged += new System.EventHandler(this.cbxMemeMode_CheckedChanged);
            // 
            // cbxSwagModeActivate
            // 
            this.cbxSwagModeActivate.AutoSize = true;
            this.cbxSwagModeActivate.BackColor = System.Drawing.Color.Transparent;
            this.cbxSwagModeActivate.Enabled = false;
            this.cbxSwagModeActivate.ForeColor = System.Drawing.Color.Red;
            this.cbxSwagModeActivate.Location = new System.Drawing.Point(36, 304);
            this.cbxSwagModeActivate.Name = "cbxSwagModeActivate";
            this.cbxSwagModeActivate.Size = new System.Drawing.Size(210, 17);
            this.cbxSwagModeActivate.TabIndex = 13;
            this.cbxSwagModeActivate.Text = "CTRL+NUM4: ALWAYS GET #SWAG";
            this.cbxSwagModeActivate.UseVisualStyleBackColor = false;
            this.cbxSwagModeActivate.CheckedChanged += new System.EventHandler(this.cbxSwagModeActivate_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.BackgroundImage = global::re5gir489.Properties.Resources.Resident_Evil_5_Box_Artwork;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(301, 332);
            this.Controls.Add(this.cbxSwagModeActivate);
            this.Controls.Add(this.cbxMemeMode);
            this.Controls.Add(this.cbxSwagBucks);
            this.Controls.Add(this.cbxAmmo);
            this.Controls.Add(this.grpWpnStat);
            this.Controls.Add(this.txtValue);
            this.Controls.Add(this.lblValue);
            this.Controls.Add(this.lblItem);
            this.Controls.Add(this.cbxItemType);
            this.Controls.Add(this.lblWaiting);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Resident Evil 5 Inventory Editor +4";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.grpWpnStat.ResumeLayout(false);
            this.grpWpnStat.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer tmrMain;
        private System.Windows.Forms.Label lblWaiting;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ComboBox cbxItemType;
        private System.Windows.Forms.Label lblItem;
        private System.Windows.Forms.ToolStripMenuItem howDoIFuckingWorkThisThingToolStripMenuItem;
        private System.Windows.Forms.Label lblValue;
        private System.Windows.Forms.TextBox txtValue;
        private System.Windows.Forms.Label lblDamage;
        private System.Windows.Forms.GroupBox grpWpnStat;
        private System.Windows.Forms.TextBox txtCrit;
        private System.Windows.Forms.Label lblCrit;
        private System.Windows.Forms.TextBox txtCapMax;
        private System.Windows.Forms.Label lblCapMax;
        private System.Windows.Forms.TextBox txtCapacity;
        private System.Windows.Forms.Label lblCapacity;
        private System.Windows.Forms.TextBox txtReloadMax;
        private System.Windows.Forms.Label lblReloadMax;
        private System.Windows.Forms.TextBox txtReloadSpeed;
        private System.Windows.Forms.Label lblReload;
        private System.Windows.Forms.TextBox txtDmgMax;
        private System.Windows.Forms.Label lblDamageMax;
        private System.Windows.Forms.TextBox txtDamage;
        private System.Windows.Forms.TextBox txtPiercing;
        private System.Windows.Forms.Label lblPiercing;
        private System.Windows.Forms.CheckBox cbxAmmo;
        private System.Windows.Forms.CheckBox cbxSwagBucks;
        private System.Windows.Forms.CheckBox cbxMemeMode;
        private System.Windows.Forms.CheckBox cbxSwagModeActivate;
    }
}

