﻿namespace re5gir489
{
    partial class HelpPlox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblExplaination = new System.Windows.Forms.LinkLabel();
            this.btnCloseHelp = new System.Windows.Forms.Button();
            this.lblExplinationNonLink = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblExplaination
            // 
            this.lblExplaination.AutoSize = true;
            this.lblExplaination.LinkArea = new System.Windows.Forms.LinkArea(0, 255);
            this.lblExplaination.Location = new System.Drawing.Point(41, 35);
            this.lblExplaination.Name = "lblExplaination";
            this.lblExplaination.Size = new System.Drawing.Size(233, 55);
            this.lblExplaination.TabIndex = 0;
            this.lblExplaination.TabStop = true;
            this.lblExplaination.Text = "Inventory Editor: Select the item you\r\nwant to manipulate in the organize menu.\r\n" +
    "ALT+TAB to the trainer and manipulate it.\r\nOnce done, send the item to Chris or " +
    "Sheva.\r\n";
            this.lblExplaination.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblExplaination.UseCompatibleTextRendering = true;
            this.lblExplaination.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblExplaination_LinkClicked);
            // 
            // btnCloseHelp
            // 
            this.btnCloseHelp.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCloseHelp.Location = new System.Drawing.Point(96, 195);
            this.btnCloseHelp.Name = "btnCloseHelp";
            this.btnCloseHelp.Size = new System.Drawing.Size(89, 23);
            this.btnCloseHelp.TabIndex = 2;
            this.btnCloseHelp.Text = "OK";
            this.btnCloseHelp.UseVisualStyleBackColor = true;
            this.btnCloseHelp.Click += new System.EventHandler(this.btnCloseHelp_Click);
            // 
            // lblExplinationNonLink
            // 
            this.lblExplinationNonLink.AutoSize = true;
            this.lblExplinationNonLink.Location = new System.Drawing.Point(41, 98);
            this.lblExplinationNonLink.Name = "lblExplinationNonLink";
            this.lblExplinationNonLink.Size = new System.Drawing.Size(211, 39);
            this.lblExplinationNonLink.TabIndex = 3;
            this.lblExplinationNonLink.Text = "3SP00KY5MEME MODE: Godmode\r\n\r\nALWAYS GET #SWAG: Always get S rank";
            this.lblExplinationNonLink.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // HelpPlox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCloseHelp;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.ControlBox = false;
            this.Controls.Add(this.lblExplinationNonLink);
            this.Controls.Add(this.btnCloseHelp);
            this.Controls.Add(this.lblExplaination);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "HelpPlox";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCloseHelp;
        private System.Windows.Forms.LinkLabel lblExplaination;
        private System.Windows.Forms.Label lblExplinationNonLink;
    }
}