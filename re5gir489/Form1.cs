﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using SharpMik.Player;
using SharpMik.Drivers;

namespace re5gir489
{
    public partial class Form1 : Form
    {
        [DllImport("kernel32.dll")]
        public static extern IntPtr OpenProcess(int dwDesiredAccess, bool bInheritHandle, int dwProcessId);

        [DllImport("kernel32.dll")]
        public static extern bool ReadProcessMemory(int hProcess, int lpBaseAddress, byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesRead);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool WriteProcessMemory(int hProcess, int lpBaseAddress, byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesWritten);

        [DllImport("winmm.DLL", EntryPoint = "PlaySound", SetLastError = true, CharSet = CharSet.Unicode, ThrowOnUnmappableChar = true)]
        private static extern bool PlaySound(string szSound, System.IntPtr hMod, PlaySoundFlags flags);

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetWindowThreadProcessId(IntPtr handle, out int processId);

        static int re5procId = 0;

        public static bool IsRE5Focused()
        {
            var activatedHandle = GetForegroundWindow();
            if (activatedHandle == IntPtr.Zero)
            {
                return false;       // No window is currently activated
            }
            
            int activeProcId;
            GetWindowThreadProcessId(activatedHandle, out activeProcId);

            return activeProcId == re5procId;
        }


        [Flags]
        public enum AllocationType
        {
            Commit = 0x1000,
            Reserve = 0x2000,
            Decommit = 0x4000,
            Release = 0x8000,
            Reset = 0x80000,
            Physical = 0x400000,
            TopDown = 0x100000,
            WriteWatch = 0x200000,
            LargePages = 0x20000000
        }

        [Flags]
        public enum PlaySoundFlags : int
        {
            SND_SYNC = 0x0000,
            SND_ASYNC = 0x0001,
            SND_NODEFAULT = 0x0002,
            SND_LOOP = 0x0008,
            SND_NOSTOP = 0x0010,
            SND_NOWAIT = 0x00002000,
            SND_FILENAME = 0x00020000,
            SND_RESOURCE = 0x00040004
        }

        [Flags]
        public enum MemoryProtection
        {
            Execute = 0x10,
            ExecuteRead = 0x20,
            ExecuteReadWrite = 0x40,
            ExecuteWriteCopy = 0x80,
            NoAccess = 0x01,
            ReadOnly = 0x02,
            ReadWrite = 0x04,
            WriteCopy = 0x08,
            GuardModifierflag = 0x100,
            NoCacheModifierflag = 0x200,
            WriteCombineModifierflag = 0x400
        }

        [DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
        static extern IntPtr VirtualAllocEx(IntPtr hProcess, IntPtr lpAddress,
           uint dwSize, AllocationType flAllocationType, MemoryProtection flProtect);

        [DllImport("user32.dll")]
        public static extern short GetAsyncKeyState(int keyState);

        IntPtr processHandle;
        MikMod m_Player = new MikMod();
        IntPtr codeCave;
        int Pointer;
        bool bIsWeapon = false;
        bool bIsCTRLPressed = false;
        byte[] swagJMP = new byte[] { 0xE9, 0x00, 0x00, 0x00, 0x00, 0x90, 0x90 };
        byte[] swagModeJMP = new byte[] { 0xE9, 0x00, 0x00, 0x00, 0x00 };
        Stream _imageStream = null;
        String music;
        GlobalKeyboardHook gHook;
        IntPtr ammoLocation1;
        IntPtr ammoLocation2;
        IntPtr swagLocation;
        IntPtr swagModeLocation;
        IntPtr memeModeLocation;
        About about;
        HelpPlox help;

        public Form1()
        {
            var exists = System.Diagnostics.Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location)).Count() > 1;
            if (exists)
            {
                MessageBox.Show("An instance of Resident Evil 5 Trainer is already running.");
                this.Close();
            }
            InitializeComponent();
            initalizeForm1();
            cbxItemType.Enabled = false;
            txtValue.Enabled = false;
            txtDamage.Enabled = false;
            txtReloadSpeed.Enabled = false;
            txtCapacity.Enabled = false;
            txtCrit.Enabled = false;
            txtPiercing.Enabled = false;
            gHook = new GlobalKeyboardHook(); // Create a new GlobalKeyboardHook
            gHook.KeyDown += new KeyEventHandler(gHook_KeyDown); // Declare a KeyDown Event
            gHook.KeyUp += new KeyEventHandler(gHook_KeyUp); //Decalre a KeyUp Event
            gHook.HookedKeys.Add(Keys.LControlKey);
            gHook.HookedKeys.Add(Keys.NumPad1);
            gHook.HookedKeys.Add(Keys.NumPad2);
            gHook.HookedKeys.Add(Keys.NumPad3);
            gHook.HookedKeys.Add(Keys.NumPad4);
            gHook.Hook();
            Dictionary<string, int> userCache = new Dictionary<string, int>
            {
                {"Nothing", 0},
                {"==WEAPONS==", -1},
                {"M92F (HG)", 258},
                {"VZ61 (MG)", 259},
                {"Ithaca M37 (SG)", 260},
                {"S75 (RIF)", 261},
                {"Hand grenade", 262},
                {"Incendiary grenade", 263},
                {"Flash grenade", 264},
                {"SIG 556 (MG)", 265},
                {"Proximity bomb", 266},
                {"S&W M29 (MAG)", 267},
                {"Rocket launcher", 269},
                {"Longbow", 271},
                {"H&K P8 (HG)", 272},
                {"SIG P226 (HG)", 273},
                {"H&K MP5 (MG)", 275},
                {"Gatling Gun", 277},
                {"M3 (SG)", 278},
                {"Jail Breaker (SG)", 279},
                {"Hydra (SG)", 281},
                {"L. Hawk (MAG)", 282},
                {"S&W 500 (MAG)", 283},
                {"H&K PSG-1 (RIF)", 284},
                {"AK-47 (MG)", 285},
                {"M93R (HG)", 286},
                {"Dragunov SVD (RIF)", 288},
                {"Stun Rod", 290},
                {"G. Launcher (EXP)", 293},
                {"G. Launcher (ACD)", 294},
                {"G. Launcher (ICE)", 295},
                {"G. Launcher (FLM)", 313},
                {"G. Launcher (FLS)", 314},
                {"G. Launcher (ELC)", 315},
                {"==AMMO==", -2},
                {"Handgun ammo", 513},
                {"Machine gun ammo", 514},
                {"Shotgun shells", 515},
                {"Rifle ammo", 516},
                {"Explosive rounds", 518},
                {"Acid rounds", 519},
                {"Nitrogen rounds", 520},
                {"Magnum rounds", 521},
                {"Flame rounds", 526},
                {"Flash rounds", 527},
                {"Electric rounds", 528},
                {"==AID==", -3},
                {"Egg (Rotten)", 310},
                {"Egg (White)", 316},
                {"Egg (Brown)", 317},
                {"Egg (Gold)", 318},
                {"First Aid Spray", 772},
                {"Herb (Green)", 773},
                {"Herb (Red)", 774},
                {"Herb (G+G)", 775},
                {"Herb (G+R)", 777},
                {"Melee Vest", 1537},
                {"Bulletproof Vest", 1542}
            };
            cbxItemType.DataSource = new BindingSource(userCache, null);
            cbxItemType.DisplayMember = "Key";
            cbxItemType.ValueMember = "Value";
            cbxItemType.SelectedIndex = 0;
        }
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (about == null || about.Visible == false)
            {
                about = new About();
                about.lblMusic.Text = music;
                about.Location = new Point(this.Location.X + 12, this.Location.Y + 75);
                about.ShowDialog();
            }
        }

        private void howDoIFuckingWorkThisThingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (help == null || help.Visible == false)
            {
                help = new HelpPlox();
                help.Location = new Point(this.Location.X + 12, this.Location.Y + 75);
                help.ShowDialog();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void initalizeForm1()
        {
            m_Player.Init<NaudioDriver>("");
            Assembly _assembly = Assembly.GetExecutingAssembly();
            if (_imageStream == null)
            {
                Random rnd = new Random();
                if (rnd.Next(0, 2) == 1)
                {
                    _imageStream = _assembly.GetManifestResourceStream("re5gir489.PMCHIP.S3M");
                    music = "Music: PMCHIP.S3M";
                }
                else
                {
                    _imageStream = _assembly.GetManifestResourceStream("re5gir489.lhsndfs-underpl_keygen.xm");
                    music = "Music: lhsndfs-underpl_keygen.xm";
                }
            }
            cbxItemType.SelectedValue = 0;
            cbxItemType.Enabled = false;
            txtValue.Enabled = false;
            cbxAmmo.Enabled = false;
            cbxSwagBucks.Enabled = false;
            cbxMemeMode.Enabled = false;
            cbxSwagModeActivate.Enabled = false;
            cbxAmmo.Checked = false;
            cbxSwagBucks.Checked = false;
            cbxMemeMode.Checked = false;
            cbxSwagModeActivate.Checked = false;
            lblWaiting.Text = "Waiting for game";
            txtValue.Text = "";
            lblWaiting.ForeColor = Color.Red;
            SharpMik.Module m_Mod = m_Player.LoadModule(_imageStream);
            m_Mod.wrap = true;
            m_Player.Play(m_Mod);
        }

        [Flags]
        public enum SnapshotFlags : uint
        {
            HeapList = 0x00000001,
            Process = 0x00000002,
            Thread = 0x00000004,
            Module = 0x00000008,
            Module32 = 0x00000010,
            Inherit = 0x80000000,
            All = 0x0000001F
        }

        [StructLayoutAttribute(LayoutKind.Sequential)]
        public struct MODULEENTRY32
        {
            public uint dwSize;
            public uint th32ModuleID;
            public uint th32ProcessID;
            public uint GlblcntUsage;
            public uint ProccntUsage;
            public IntPtr modBaseAddr;
            public uint modBaseSize;
            IntPtr hModule;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szModule;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string szExePath;
        };

        [DllImport("kernel32.dll", SetLastError = true)]
        static public extern bool CloseHandle(IntPtr hHandle);

        [DllImport("kernel32.dll")]
        static public extern bool Module32First(IntPtr hSnapshot, ref MODULEENTRY32 lpme);

        [DllImport("kernel32.dll")]
        static public extern bool Module32Next(IntPtr hSnapshot, ref MODULEENTRY32 lpme);

        [DllImport("kernel32.dll", SetLastError = true)]
        static public extern IntPtr CreateToolhelp32Snapshot(SnapshotFlags dwFlags, uint th32ProcessID);

        IntPtr GetModule(uint procID, String name)
        {
            var snapshot = CreateToolhelp32Snapshot(SnapshotFlags.Module | SnapshotFlags.Module32, procID);
            MODULEENTRY32 mod = new MODULEENTRY32()
            {
                dwSize = (uint)Marshal.SizeOf(typeof(MODULEENTRY32))
            };
            if (!Module32First(snapshot, ref mod))
                return IntPtr.Zero;
            do
            {
                if (mod.szModule == name)
                {
                    return mod.modBaseAddr;
                }
            }
            while (Module32Next(snapshot, ref mod));
            return IntPtr.Zero;
        }

        public void gHook_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.LControlKey)
            {
                bIsCTRLPressed = true;
            }
            if (bIsCTRLPressed)
            {
                if (e.KeyCode == Keys.NumPad1)
                {
                    if (cbxAmmo.Enabled == true)
                    {
                        cbxAmmo.Checked = !cbxAmmo.Checked;
                    }
                }
                if (e.KeyCode == Keys.NumPad2)
                {
                    if (cbxAmmo.Enabled == true)
                    {
                        cbxSwagBucks.Checked = !cbxSwagBucks.Checked;
                    }
                }
                if (e.KeyCode == Keys.NumPad3)
                {
                    if (cbxMemeMode.Enabled == true)
                    {
                        cbxMemeMode.Checked = !cbxMemeMode.Checked;
                    }
                }
                if (e.KeyCode == Keys.NumPad4)
                {
                    if (cbxSwagModeActivate.Enabled == true)
                    {
                        cbxSwagModeActivate.Checked = !cbxSwagModeActivate.Checked;
                    }
                }
            }
        }

        public void gHook_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.LControlKey)
            {
                bIsCTRLPressed = false;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Process[] process = Process.GetProcessesByName("re5dx9");
            if (process.Length == 0)
            {
                processHandle = IntPtr.Zero;
                if (m_Player.IsPlaying() == false)
                {
                    initalizeForm1();
                }
                return;
            }
            if (processHandle.Equals(IntPtr.Zero))
            {
                re5procId = process[0].Id;
                processHandle = OpenProcess(0x38, false, re5procId);
                lblWaiting.Text = "Found Game";
                lblWaiting.ForeColor = Color.Green;
                m_Player.Stop();
                IntPtr xlive = GetModule((uint)re5procId, "xlive.dll");
                int bytesWritten = 0;
                if (xlive != IntPtr.Zero)
                {
                    SigScan xliveSigInstance = new SigScan(process[0], xlive, 0xABFFFF);
                    IntPtr scannerSignature = xliveSigInstance.FindPattern(new byte[] { 0x74, 0x14, 0xFF, 0x75, 0x14, 0x8B, 0xCE }, "xxxxxxx", 0);
                    if (scannerSignature != IntPtr.Zero)
                    {
                        byte[] geriGFWLBypassOpCodes = { 0xEB };
                        WriteProcessMemory((int)processHandle, scannerSignature.ToInt32(), geriGFWLBypassOpCodes, geriGFWLBypassOpCodes.Length, ref bytesWritten);
                    }
                }
                codeCave = VirtualAllocEx(processHandle, IntPtr.Zero, 0xB0, AllocationType.Commit | AllocationType.Reserve, MemoryProtection.ExecuteReadWrite);
                SigScan sigScanInstance = new SigScan(process[0], new IntPtr(0x401000), 0xF00000);
                IntPtr inventoryLocation = sigScanInstance.FindPattern(new byte[] { 0x66, 0x83, 0x7D, 0x00, 0x00, 0x74, 0x25 }, "xxx??xx", 0);
                IntPtr equipedLocation = sigScanInstance.FindPattern(new byte[] { 0x8B, 0x44, 0x24, 0x3C, 0x66, 0x83, 0x38, 0x00, 0x0F, 0x84 }, "xxxxxxxxxx", 0);
                ammoLocation1 = sigScanInstance.FindPattern(new byte[] { 0x0F, 0x8F, 0x00, 0x00, 0x00, 0x00, 0x8B, 0x7E, 0x2C }, "xx????xxx", 0);
                ammoLocation2 = sigScanInstance.FindPattern(new byte[] { 0x29, 0x6E, 0x08, 0x5F, 0x5E, 0x5D }, "xxxxxx", 0);
                swagLocation = sigScanInstance.FindPattern(new byte[] { 0x8B, 0x94, 0x01, 0x00, 0x00, 0x00, 0x00, 0x89, 0x56, 0x78 }, "xxx????xxx", 0);
                memeModeLocation = sigScanInstance.FindPattern(new byte[] { 0x66, 0x83, 0xB9, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0x9E, 0xC0 }, "xxx?????xxx", 0);
                swagModeLocation = sigScanInstance.FindPattern(new byte[] { 0xF3, 0x0F, 0x10, 0x04, 0xC1, 0xF3, 0x0F, 0x58, 0x44, 0x24 }, "xxxxxxxxxx", 0);
                if (inventoryLocation == IntPtr.Zero)
                {
                    MessageBox.Show("Failed Inventory Signature");
                    Application.Exit();
                }
                if (equipedLocation == IntPtr.Zero)
                {
                    MessageBox.Show("Failed Equipped Signature");
                    Application.Exit();
                }
                if (ammoLocation1 == IntPtr.Zero || ammoLocation2 == IntPtr.Zero)
                {
                    MessageBox.Show("Failed Ammo Signature " + ammoLocation1.ToString("X4") + " " + ammoLocation2.ToString("X4"));
                    Application.Exit();
                }
                if (swagLocation == IntPtr.Zero)
                {
                    MessageBox.Show("Failed Swag Signature");
                    Application.Exit();
                }
                if (memeModeLocation == IntPtr.Zero)
                {
                    MessageBox.Show("Failed Spooky Signature");
                    Application.Exit();
                }
                if (swagModeLocation == IntPtr.Zero)
                {
                    MessageBox.Show("Failed SwagMode Signature");
                    Application.Exit();
                }
                byte[] inventoryBytes = new byte[] { 0x89, 0x2D, 0x00, 0x00, 0x00, 0x00, 0x66, 0x83, 0x7D, 0x00, 0x00, 0xE9, 0x00, 0x00, 0x00, 0x00 };
                byte[] equippedBytes = new byte[] { 0x8B, 0x44, 0x24, 0x3C, 0x89, 0x05, 0x00, 0x00, 0x00, 0x00, 0x66, 0x83, 0x38, 0x00, 0xE9, 0x00, 0x00, 0x00, 0x00 };
                byte[] swagBytes = new byte[] { 0xBA, 0x7A, 0x46, 0x23, 0x04, 0x89, 0x94, 0x08, 0xC0, 0x01, 0x00, 0x00, 0xE9, 0x00, 0x00, 0x00, 0x00 };
                byte[] swagModeBytes = new byte[] { 0x3D, 0x3D, 0x08, 0x00, 0x00, 0x75, 0x67, 0x56, 0x8B, 0xF1, 0x01, 0xC6, 0x01, 0xC6, 0x01, 0xC6, 0x01, 0xC6, 0x01, 0xC6, 0x01, 0xC6, 0x01, 0xC6, 0x01, 0xC6, 0xC7, 0x46, 0xF4, 0x7A, 0x46, 0x23, 0x04, 0xC7, 0x46, 0xF8, 0x45, 0x00, 0x00, 0x00, 0xC7, 0x46, 0xFC, 0x45, 0x00, 0x00, 0x00, 0xC7, 0x06, 0x00, 0x00, 0x82, 0x43, 0xC7, 0x46, 0x04, 0x01, 0x00, 0x00, 0x00, 0xC7, 0x46, 0x08, 0x00, 0x00, 0x00, 0x00, 0xC7, 0x46, 0x0C, 0x7A, 0x46, 0x23, 0x04, 0xC7, 0x46, 0x10, 0x45, 0x00, 0x00, 0x00, 0xC7, 0x46, 0x14, 0x45, 0x00, 0x00, 0x00, 0xC7, 0x46, 0x18, 0x00, 0x00, 0x82, 0x43, 0xC7, 0x46, 0x1C, 0x01, 0x00, 0x00, 0x00, 0xC7, 0x46, 0x20, 0x00, 0x00, 0x00, 0x00, 0x5E, 0xF3, 0x0F, 0x10, 0x04, 0xC1, 0xE9, 0x00, 0x00, 0x00, 0x00 };
                byte[] addressByte = BitConverter.GetBytes(codeCave.ToInt32() + 0xAC);
                inventoryBytes[2] = addressByte[0];
                inventoryBytes[3] = addressByte[1];
                inventoryBytes[4] = addressByte[2];
                inventoryBytes[5] = addressByte[3];
                equippedBytes[6] = addressByte[0];
                equippedBytes[7] = addressByte[1];
                equippedBytes[8] = addressByte[2];
                equippedBytes[9] = addressByte[3];
                int inventoryAddress = inventoryLocation.ToInt32() + 0x5;
                int equippedAddress = equipedLocation.ToInt32() + 0x8;
                int swagAddress = swagLocation.ToInt32() + 0x7;
                int swagModeAddress = swagModeLocation.ToInt32() + 0x5;
                int codecaveAddress = codeCave.ToInt32() + 0xB;
                int codecaveAddress2 = codeCave.ToInt32() + 0x1E;
                int codecaveAddress3 = codeCave.ToInt32() + 0x2F;
                int codecaveAddress4 = codeCave.ToInt32() + 0xA7;
                addressByte = BitConverter.GetBytes((inventoryAddress - codecaveAddress) - 5);
                inventoryBytes[12] = addressByte[0];
                inventoryBytes[13] = addressByte[1];
                inventoryBytes[14] = addressByte[2];
                inventoryBytes[15] = addressByte[3];
                addressByte = BitConverter.GetBytes((equippedAddress - codecaveAddress2) - 5);
                equippedBytes[15] = addressByte[0];
                equippedBytes[16] = addressByte[1];
                equippedBytes[17] = addressByte[2];
                equippedBytes[18] = addressByte[3];
                addressByte = BitConverter.GetBytes((swagAddress - codecaveAddress3) - 5);
                swagBytes[13] = addressByte[0];
                swagBytes[14] = addressByte[1];
                swagBytes[15] = addressByte[2];
                swagBytes[16] = addressByte[3];
                addressByte = BitConverter.GetBytes((swagModeAddress - codecaveAddress4) - 5);
                swagModeBytes[116] = addressByte[0];
                swagModeBytes[117] = addressByte[1];
                swagModeBytes[118] = addressByte[2];
                swagModeBytes[119] = addressByte[3];
                byte[] genericJMP = new byte[] { 0xE9, 0x00, 0x00, 0x00, 0x00 };
                addressByte = BitConverter.GetBytes((codeCave.ToInt32() - inventoryLocation.ToInt32()) - 5);
                genericJMP[1] = addressByte[0];
                genericJMP[2] = addressByte[1];
                genericJMP[3] = addressByte[2];
                genericJMP[4] = addressByte[3];
                WriteProcessMemory((int)processHandle, codeCave.ToInt32(), inventoryBytes, inventoryBytes.Length, ref bytesWritten);
                WriteProcessMemory((int)processHandle, codeCave.ToInt32() + 0x10, equippedBytes, equippedBytes.Length, ref bytesWritten);
                WriteProcessMemory((int)processHandle, codeCave.ToInt32() + 0x23, swagBytes, swagBytes.Length, ref bytesWritten);
                WriteProcessMemory((int)processHandle, codeCave.ToInt32() + 0x34, swagModeBytes, swagModeBytes.Length, ref bytesWritten);
                WriteProcessMemory((int)processHandle, inventoryLocation.ToInt32(), genericJMP, genericJMP.Length, ref bytesWritten);
                addressByte = BitConverter.GetBytes(((codeCave.ToInt32() + 0x10) - (equipedLocation.ToInt32())) - 5);
                byte[] genericJMP2 = new byte[] { 0xE9, 0x00, 0x00, 0x00, 0x00, 0x90, 0x90, 0x90 };
                genericJMP2[1] = addressByte[0];
                genericJMP2[2] = addressByte[1];
                genericJMP2[3] = addressByte[2];
                genericJMP2[4] = addressByte[3];
                WriteProcessMemory((int)processHandle, equipedLocation.ToInt32(), genericJMP2, genericJMP2.Length, ref bytesWritten);
                addressByte = BitConverter.GetBytes(((codeCave.ToInt32() + 0x23) - (swagLocation.ToInt32())) - 5);
                swagJMP[1] = addressByte[0];
                swagJMP[2] = addressByte[1];
                swagJMP[3] = addressByte[2];
                swagJMP[4] = addressByte[3];
                addressByte = BitConverter.GetBytes(((codeCave.ToInt32() + 0x34) - (swagModeLocation.ToInt32())) - 5);
                swagModeJMP[1] = addressByte[0];
                swagModeJMP[2] = addressByte[1];
                swagModeJMP[3] = addressByte[2];
                swagModeJMP[4] = addressByte[3];
                cbxItemType.Enabled = true;
                txtValue.Enabled = true;
                txtDamage.Enabled = true;
                txtReloadSpeed.Enabled = true;
                txtCapacity.Enabled = true;
                txtCrit.Enabled = true;
                txtPiercing.Enabled = true;
                cbxSwagBucks.Enabled = true;
                cbxAmmo.Enabled = true;
                cbxMemeMode.Enabled = true;
                cbxSwagModeActivate.Enabled = true;
            }
            else if (!IsRE5Focused())
            {
                byte[] buffer = new byte[4];
                int bytesRead = 0;
                ReadProcessMemory((int)processHandle, codeCave.ToInt32() + 0xAC, buffer, 4, ref bytesRead);
                Pointer = BitConverter.ToInt32(buffer, 0);
                ReadProcessMemory((int)processHandle, Pointer, buffer, 2, ref bytesRead);
                int itemType = BitConverter.ToInt16(buffer, 0);
                if (cbxItemType.DroppedDown == false)
                {
                    cbxItemType.SelectedValue = itemType;
                }
                if (itemType > 1542 || itemType < 0)
                {
                    cbxItemType.SelectedValue = 0;
                    txtValue.Text = "";
                }
                else
                {
                    ReadProcessMemory((int)processHandle, Pointer + 2, buffer, 2, ref bytesRead);
                    UInt16 value = BitConverter.ToUInt16(buffer, 0);
                    if (txtValue.Text != value.ToString() && value >= 0)
                    {
                        txtValue.Text = value.ToString();
                    }
                }
                bIsWeapon = (itemType >= 258 && itemType <= 315);
                if (bIsWeapon == true)
                {
                    txtDamage.Enabled = true;
                    txtReloadSpeed.Enabled = true;
                    txtCapacity.Enabled = true;
                    txtCrit.Enabled = true;
                    txtPiercing.Enabled = true;
                    ReadProcessMemory((int)processHandle, Pointer + 0x40, buffer, 1, ref bytesRead);
                    if (txtDmgMax.Text != buffer[0].ToString())
                    {
                        txtDmgMax.Text = buffer[0].ToString();
                    }
                    ReadProcessMemory((int)processHandle, Pointer + 0x30, buffer, 1, ref bytesRead);
                    if (txtDamage.Text != buffer[0].ToString())
                    {
                        txtDamage.Text = buffer[0].ToString();
                    }
                    ReadProcessMemory((int)processHandle, Pointer + 0x41, buffer, 1, ref bytesRead);
                    int reloadMax = BitConverter.ToInt16(buffer, 0);
                    if (txtReloadMax.Text != buffer[0].ToString())
                    {
                        txtReloadMax.Text = buffer[0].ToString();
                    }
                    ReadProcessMemory((int)processHandle, Pointer + 0x31, buffer, 1, ref bytesRead);
                    if (txtReloadSpeed.Text != buffer[0].ToString())
                    {
                        txtReloadSpeed.Text = buffer[0].ToString();
                    }
                    ReadProcessMemory((int)processHandle, Pointer + 0x42, buffer, 1, ref bytesRead);
                    if (txtCapMax.Text != buffer[0].ToString())
                    {
                        txtCapMax.Text = buffer[0].ToString();
                    }
                    ReadProcessMemory((int)processHandle, Pointer + 0x32, buffer, 1, ref bytesRead);
                    if (txtCapacity.Text != buffer[0].ToString())
                    {
                        txtCapacity.Text = buffer[0].ToString();
                    }
                    ReadProcessMemory((int)processHandle, Pointer + 0x33, buffer, 1, ref bytesRead);
                    if (txtCrit.Text != buffer[0].ToString())
                    {
                        txtCrit.Text = buffer[0].ToString();
                    }
                    ReadProcessMemory((int)processHandle, Pointer + 0x34, buffer, 1, ref bytesRead);
                    if (txtPiercing.Text != buffer[0].ToString())
                    {
                        txtPiercing.Text = buffer[0].ToString();
                    }
                }
                else
                {
                    txtDamage.Text = "";
                    txtDmgMax.Text = "";
                    txtReloadSpeed.Text = "";
                    txtReloadMax.Text = "";
                    txtCapacity.Text = "";
                    txtCapMax.Text = "";
                    txtCrit.Text = "";
                    txtPiercing.Text = "";
                    txtDamage.Enabled = false;
                    txtReloadSpeed.Enabled = false;
                    txtCapacity.Enabled = false;
                    txtCrit.Enabled = false;
                    txtPiercing.Enabled = false;
                }
            }
        }

        private void cbxItemType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (Pointer.Equals(null) || (int)cbxItemType.SelectedValue <= 0)
                return;

            byte[] selectedIndex = BitConverter.GetBytes((short)(int)cbxItemType.SelectedValue);
            int bytesWritten = 0;
            WriteProcessMemory((int)processHandle, Pointer, selectedIndex, selectedIndex.Length, ref bytesWritten);
        }

        private void txtValue_TextChanged(object sender, EventArgs e)
        {
            if (txtValue.Text.Length == 0 || cbxItemType.SelectedValue == null || cbxItemType.SelectedValue.Equals(0) )
            {
                txtValue.Text = "";
                return;
            }
            UInt16 valueParse;
            if (UInt16.TryParse(txtValue.Text, out valueParse))
            {
                if (valueParse < 0)
                {
                    MessageBox.Show("Only enter numbers between " + UInt16.MinValue.ToString() + " and " + UInt16.MaxValue.ToString());
                    txtValue.Text = "";
                    return;
                }
                byte[] value = BitConverter.GetBytes(valueParse);
                int bytesWritten = 0;
                WriteProcessMemory((int)processHandle, Pointer + 2, value, value.Length, ref bytesWritten);
            }
            else
            {
                MessageBox.Show("Only enter numbers between " + UInt16.MinValue.ToString() + " and " + UInt16.MaxValue.ToString());
                txtValue.Text = "";
            }
        }

        private void txtDamage_TextChanged(object sender, EventArgs e)
        {
            if (txtDamage.Text.Length == 0 || bIsWeapon == false)
            {
                return;
            }
            Byte damageParse;
            if (Byte.TryParse(txtDamage.Text, out damageParse))
            {
                Byte damageMaxParse;
                if (Byte.TryParse(txtDmgMax.Text, out damageMaxParse))
                {
                    if (damageParse > damageMaxParse)
                    {
                        MessageBox.Show("Invalid Damage Entered.");
                        return;
                    }
                    else
                    {
                        byte[] value = { 0x00 };
                        value[0] = damageParse;
                        int bytesWritten = 0;
                        WriteProcessMemory((int)processHandle, Pointer + 0x30, value, value.Length, ref bytesWritten);
                    }
                }
            }
        }

        private void txtReloadSpeed_TextChanged(object sender, EventArgs e)
        {
            if (txtReloadSpeed.Text.Length == 0 || bIsWeapon == false)
            {
                return;
            }
            Byte reloadParse;
            if (Byte.TryParse(txtReloadSpeed.Text, out reloadParse))
            {
                Byte reloadMaxParse;
                if (Byte.TryParse(txtReloadMax.Text, out reloadMaxParse))
                {
                    if (reloadParse > reloadMaxParse)
                    {
                        MessageBox.Show("Invalid Reload Entered.");
                        return;
                    }
                    else
                    {
                        byte[] value = { 0x00 };
                        value[0] = reloadParse;
                        int bytesWritten = 0;
                        WriteProcessMemory((int)processHandle, Pointer + 0x31, value, value.Length, ref bytesWritten);
                    }
                }
            }
        }

        private void txtCapacity_TextChanged(object sender, EventArgs e)
        {
            if (txtCapacity.Text.Length == 0 || bIsWeapon == false)
            {
                return;
            }
            Byte capacityParse;
            if (Byte.TryParse(txtCapacity.Text, out capacityParse))
            {
                Byte capacityMaxParse;
                if (Byte.TryParse(txtCapMax.Text, out capacityMaxParse))
                {
                    if (capacityParse > capacityMaxParse)
                    {
                        MessageBox.Show("Invalid Capacity Entered.");
                        return;
                    }
                    else
                    {
                        byte[] value = { 0x00 };
                        value[0] = capacityParse;
                        int bytesWritten = 0;
                        WriteProcessMemory((int)processHandle, Pointer + 0x32, value, value.Length, ref bytesWritten);
                    }
                }
            }
        }

        private void txtCrit_TextChanged(object sender, EventArgs e)
        {
            if (txtCrit.Text.Length == 0 || bIsWeapon == false)
            {
                return;
            }
            Byte critParse;
            if (Byte.TryParse(txtCrit.Text, out critParse))
            {
                if (critParse > 3)
                {
                    MessageBox.Show("Invalid Crit Entered.");
                    return;
                }
                else
                {
                    byte[] value = { 0x00 };
                    value[0] = critParse;
                    int bytesWritten = 0;
                    WriteProcessMemory((int)processHandle, Pointer + 0x33, value, value.Length, ref bytesWritten);
                }
            }
        }

        private void txtPiercing_TextChanged(object sender, EventArgs e)
        {
            if (txtPiercing.Text.Length == 0 || bIsWeapon == false)
            {
                return;
            }
            Byte pierceParse;
            if (Byte.TryParse(txtPiercing.Text, out pierceParse))
            {
                if (pierceParse > 3)
                {
                    MessageBox.Show("Invalid Pierce Entered.");
                    return;
                }
                else
                {
                    byte[] value = { 0x00 };
                    value[0] = pierceParse;
                    int bytesWritten = 0;
                    WriteProcessMemory((int)processHandle, Pointer + 0x34, value, value.Length, ref bytesWritten);
                }
            }
        }

        private void cbxAmmo_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxAmmo.Enabled == true)
            {
                if (cbxAmmo.Checked == true)
                {
                    byte[] ammo1opcodes = { 0x90, 0xE9 };
                    int bytesWritten = 0;
                    WriteProcessMemory((int)processHandle, ammoLocation1.ToInt32(), ammo1opcodes, ammo1opcodes.Length, ref bytesWritten);
                    byte[] ammo2opcodes = { 0x90, 0x90, 0x90 };
                    WriteProcessMemory((int)processHandle, ammoLocation2.ToInt32(), ammo2opcodes, ammo2opcodes.Length, ref bytesWritten);
                    cbxAmmo.ForeColor = Color.Lime;
                    PlaySound("Media\\Windows Exclamation.wav", new System.IntPtr(), PlaySoundFlags.SND_ASYNC);
                }
                else
                {
                    byte[] ammo1opcodes = { 0x0F, 0x8F };
                    int bytesWritten = 0;
                    WriteProcessMemory((int)processHandle, ammoLocation1.ToInt32(), ammo1opcodes, ammo1opcodes.Length, ref bytesWritten);
                    byte[] ammo2opcodes = { 0x29, 0x6E, 0x08 };
                    WriteProcessMemory((int)processHandle, ammoLocation2.ToInt32(), ammo2opcodes, ammo2opcodes.Length, ref bytesWritten);
                    cbxAmmo.ForeColor = Color.Red;
                    PlaySound("Media\\Windows Ding.wav", new System.IntPtr(), PlaySoundFlags.SND_ASYNC);
                }
            }
        }

        private void cbxSwagBucks_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxSwagBucks.Enabled == true)
            {
                if (cbxSwagBucks.Checked == true)
                {
                    int bytesWritten = 0;
                    WriteProcessMemory((int)processHandle, swagLocation.ToInt32(), swagJMP, swagJMP.Length, ref bytesWritten);
                    cbxSwagBucks.ForeColor = Color.Lime;
                    PlaySound("Media\\Windows Exclamation.wav", new System.IntPtr(), PlaySoundFlags.SND_ASYNC);
                }
                else
                {
                    byte[] swagOpCodes = { 0x8B, 0x94, 0x01, 0xC0, 0x01, 0x00, 0x00 };
                    int bytesWritten = 0;
                    WriteProcessMemory((int)processHandle, swagLocation.ToInt32(), swagOpCodes, swagOpCodes.Length, ref bytesWritten);
                    cbxSwagBucks.ForeColor = Color.Red;
                    PlaySound("Media\\Windows Ding.wav", new System.IntPtr(), PlaySoundFlags.SND_ASYNC);
                }
            }
        }

        private void cbxMemeMode_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxMemeMode.Enabled == true)
            {
                if (cbxMemeMode.Checked == true)
                {
                    byte[] memeCodesLeXD = { 0x66, 0xC7, 0x81, 0x64, 0x13, 0x00, 0x00, 0xE8, 0x03, 0x30, 0xC0 };
                    int bytesWritten = 0;
                    WriteProcessMemory((int)processHandle, memeModeLocation.ToInt32(), memeCodesLeXD, memeCodesLeXD.Length, ref bytesWritten);
                    cbxMemeMode.ForeColor = Color.Lime;
                    PlaySound("Media\\Windows Exclamation.wav", new System.IntPtr(), PlaySoundFlags.SND_ASYNC);
                }
                else
                {
                    byte[] originalMemeModeOpcodes = { 0x66, 0x83, 0xB9, 0x64, 0x13, 0x00, 0x00, 0x00, 0x0F, 0x9E, 0xC0 };
                    int bytesWritten = 0;
                    WriteProcessMemory((int)processHandle, memeModeLocation.ToInt32(), originalMemeModeOpcodes, originalMemeModeOpcodes.Length, ref bytesWritten);
                    cbxMemeMode.ForeColor = Color.Red;
                    PlaySound("Media\\Windows Ding.wav", new System.IntPtr(), PlaySoundFlags.SND_ASYNC);
                }
            }
        }

        private void cbxSwagModeActivate_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxSwagModeActivate.Enabled == true)
            {
                if (cbxSwagModeActivate.Checked == true)
                {
                    int bytesWritten = 0;
                    WriteProcessMemory((int)processHandle, swagModeLocation.ToInt32(), swagModeJMP, swagModeJMP.Length, ref bytesWritten);
                    cbxSwagModeActivate.ForeColor = Color.Lime;
                    PlaySound("Media\\Windows Exclamation.wav", new System.IntPtr(), PlaySoundFlags.SND_ASYNC);
                }
                else
                {
                    byte[] swagModeOpCodes = { 0xF3, 0x0F, 0x10, 0x04, 0xC1 };
                    int bytesWritten = 0;
                    WriteProcessMemory((int)processHandle, swagModeLocation.ToInt32(), swagModeOpCodes, swagModeOpCodes.Length, ref bytesWritten);
                    cbxSwagModeActivate.ForeColor = Color.Red;
                    PlaySound("Media\\Windows Ding.wav", new System.IntPtr(), PlaySoundFlags.SND_ASYNC);
                }
            }
        }

        private void lblWaiting_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "MEMES! If you know what I meme.", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                MessageBox.Show("               ( ͡° ͜ʖ ͡°)");
            }
        }
    }
}
